#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: dash 0.4.18\n"
"Report-Msgid-Bugs-To: dash@packages.debian.org\n"
"POT-Creation-Date: 2008-03-02 21:54+0000\n"
"PO-Revision-Date: 2008-07-21 10:50+0200\n"
"Last-Translator: Thijs Kinkhorst <thijs@debian.org>\n"
"Language-Team: Debian Dutch <debian-l10n-dutch@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../dash.templates.in:1001
msgid "Install dash as /bin/sh?"
msgstr "dash als /bin/sh installeren?"

#. Type: boolean
#. Description
#: ../dash.templates.in:1001
msgid "The default /bin/sh shell on Debian and Debian-based systems is bash."
msgstr ""
"De standaard /bin/sh shell voor Debian en voor op Debian-gebaseerde systemen is bash."

#. Type: boolean
#. Description
#: ../dash.templates.in:1001
msgid ""
"However, since the default shell is required to be POSIX-compliant, any "
"shell that conforms to POSIX, such as dash, can serve as /bin/sh. You may "
"wish to do this because dash is faster and smaller than bash."
msgstr ""
"Echter, Debian-beleid eist dat elke shell die zich conformeert aan "
"POSIX, zoals dash, kan worden gebruikt voor /bin/sh. Een reden om dit te "
"overwegen is dat dash sneller en compacter is dan bash."
